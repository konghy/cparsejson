cparsejson
==========

用C语言解析 json 数据

----------

#### 说明
本示例采用的 json 解析库为 josn-c，在 ubuntu 系统及其衍生版本中可以采用如下命令安装：

> $ sudo apt-get install libjson-c-dev

如果系统无法直接安装，则可到 github 上获取源代码编译安装，该项目的 github 地址为： [http://github.com/json-c/json-c](http://github.com/json-c/json-c)


#### 编译应用程序
在编译用 json-c 库开发的应用程序时，应指定库文件名和头文件位置，例：

> $ gcc test2.c -I/usr/include/json-c -ljson-c -g -o psjson

相应的编译选项可以用如下命令查看：
> $ pkg-config --cflags json-c

> $ pkg-config --libs json-c

#### 常用函数接口简介
```c
/* Json 对象的类型: */
json_type_object, “名称/值”对的集合 Json 对象的值类型
json_type_boolean,
json_type_double,
json_type_int,
json_type_array,  “值”的集合
json_type_string

json_object_to_file(filepath, json_object); //将json_object写到文件中
json_object_from_file(filepath);  //从文件中读出json_object

json_object_new_object();   //创建一个新的json对象，生成{ }
json_object_object_add(json_object, "name", json_object_value);   //添加后{"name": "value"}
json_object_object_del(json_object, "name");   //删除一个键值对
json_object_object_foreach(json_object, key, val)  //遍历对象
 {
           printf("\t%s: %s\n", key, json_object_to_json_string(val));
}

json_object_new_array();   //生成[ ]
json_object_array_length(struct json_object *obj); //得到 json 数组的长度
json_object_array_add(json_object, json_object_new_string("value"));  //添加值
json_object_get_array_idx(json_object, i);  //得到第i的值

json_object_new_int(0);
json_object_new_string("str");
json_object_new_boolean(0);

json_object_get_type(struct json_object * this )  //获取 json 对象的类型
json_object_is_type(struct json_object * this, enum json_type type) //判断 json 对象的类型

json_object_to_json_string(json_object); //生成json字符串
```
#include <stdio.h>
#include <json.h>


/*************************************************************************
 * FuncName: main
 *    Input: None
 *   Output: None
 *  Val-Res: None
 *   Return: 0
 *    Brief: Main program body!
*************************************************************************/
int main(void)
{
	json_object *pval = NULL;
    enum json_type type;

    type = json_object_get_type(pval);
    printf("type:%d\n", type);

    return 0;
}


/************************ (C) COPYRIGHT HOUTY PRIVATE ********END OF FILE****/


#include <json.h>
#include "linkhash.h"
#include <json_object_private.h>
#include <stdio.h>


int main(int argc, char **argv)
{
    //FILE* fid=fopen(argv[1],"r");
    const char *str;
    json_object* jo;

    jo = json_object_from_file(argv[1]);
    str = json_object_to_json_string(jo);
    printf("count:%d\ntype:%d\n%s\n",jo->_ref_count,jo->o_type,str);

    struct lh_table *t = jo->o.c_object;
    struct lh_entry *en = lh_table_lookup_entry(t, "glossary");
    
    json_object *jo1 = (json_object*)(en->v);
    printf("->|%s|<-\n", json_object_to_json_string(jo1));
    //printf("%s\n",(char*)en->v);
    json_object_put(jo);

    return 0;
}


